package be.kdg.contact.view;

import be.kdg.contact.model.Message;
import be.kdg.contact.model.Messenger;

public class ContactPresenter {
    private final Messenger model;
    private final ContactView view;

    public ContactPresenter(Messenger model, ContactView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
    }

    private void addEventHandlers() {
        this.view.getSubmitButton().setOnAction(event -> {
            final String lastName = view.getLastNameInput().getText();
            final String firstName = view.getFirstNameInput().getText();
            final String email = view.getEmailInput().getText();
            final String messageBody = view.getMessageBodyInput().getText();

            Message message = new Message(lastName, firstName, email, messageBody);
            model.send(message);
            updateView();
        });
    }

    private void updateView() {
        view.clearInput();
    }
}
