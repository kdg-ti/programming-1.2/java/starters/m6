package be.kdg.students.model;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class StudentCatalog {
    private final List<Student> students;
    private final InputStream inputStream;

    public StudentCatalog() {
        this.students = new ArrayList<>();
        this.inputStream = this.getClass().getClassLoader().getResourceAsStream("students.txt");
        this.readStudents();
    }

    public List<Student> getStudents() {
        return this.students;
    }

    private void readStudents() {
        // TODO: complete this method
    }
}
